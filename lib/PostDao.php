<?php

class PostDao implements \Temma\Base\Loadable {
	private $_db;

	public function __construct(\Temma\Base\Loader $loader) {
		$this->_db = $loader->dataSources['db'];
	}
	public function getPosts($premier, $maxPages) {
        $sql = "SELECT	pos_i_id AS id,
                        pos_s_name AS nom,
                        pos_s_text AS text,
                        pos_d_date AS date
                FROM Post
                ORDER BY pos_i_id DESC
                LIMIT $premier, $maxPages";
		$posts = $this->_db->queryAll($sql);
		return ($posts);
	}

    public function add(string $name, string $post) {
        $sql = "INSERT INTO Post
                SET pos_d_date = NOW(),
                    pos_s_name = " . $this->_db->quote(trim($name)) .
                    ", pos_s_text = " . $this->_db->quote(trim($post));
        $this->_db->exec($sql);
    }

    public function count(): int
    {
        $sql = "SELECT COUNT(*) AS nbPost FROM Post";
        $count = $this->_db->queryOne($sql);
        return $count["nbPost"];
    }
}
