<html>
<head>
	<style>
		.pagination a {
			color: black;
			padding: 8px 16px;
			text-decoration: none;
		}
		.active {
			font-weight: bold;
		}
	</style>
</head>
<body>

<ul>
	{foreach $posts as $post}
		<li>{if $post.nom}{$post.nom|escape} :{/if} {$post.text|escape} ({$post.date})</li>
	{/foreach}
</ul>

<form action="/post/add" method="post">
	<input type="text" placeholder="nom" name="name"><br>
	<input type="text" placeholder="post" name="text"><br>
	<input type="submit">
</form>

<div class="pagination">
	{if ($currentPage - 1) >= 1}
		<a href="/post/list/{$currentPage - 1}">&laquo;</a>
	{/if}
	{for $page=1 to $nbrPages}
		{if $page == $currentPage}
			<span class="active">{$page}</span>
		{else}
			<a href="/post/list/{$page}">{$page}</a>
		{/if}
	{/for}
	{if ($currentPage + 1) <= $nbrPages}
		<a href="/post/list/{$currentPage + 1}">&raquo;</a>
	{/if}
</div>
</body>
</html>
