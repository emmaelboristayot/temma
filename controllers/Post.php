<?php

class Post extends \Temma\Web\Controller {

    const MAX_PAGES = 5;

	public function __invoke() {
		$this->_redirect('/post/list');
	}

    public function list(?int $page = null)
    {
        if ($page === null) {
            return $this->_redirect('/post/list/1');
        }

        $count = $this->_loader->PostDao->count();
        $premier = ($page * self::MAX_PAGES) - self::MAX_PAGES;
        $this['posts'] = $this->_loader->PostDao->getPosts($premier, self::MAX_PAGES);
        $this['nbrPages'] = ceil($count / self::MAX_PAGES);
        $this['currentPage'] = $page;
    }

    public function add() {
        $name = $_POST['name'] ?? null;
        $post = $_POST['text'] ?? null;
        if (!empty($post)) {
            $this->_loader->PostDao->add($name, $post);
        }
        $this->_redirect('/post/list');
    }

}
